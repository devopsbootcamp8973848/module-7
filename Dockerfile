FROM openjdk:23-slim
#expose the app's port
EXPOSE 8080 
#creates the same environment variables used when I stared the app manually
#removed the env var as Nana did not include them
#create a directory for the app on the container
RUN mkdir -p /opt/docker-exercises-app
#copy the jar file to the app's directry on the container
#I included a "." bewfore /build - Nana did not
COPY build/libs/docker-exercises-project-1.0-SNAPSHOT.jar /opt/docker-exercises-app

#to avoid the app running as "root", create a user, grant the user permission to /home/docker-exercises-app
#switch to the user so, the java command runs on that user's context
#create a group and user
RUN groupadd -r app-user && useradd -g app-user app-user
#make the new user the owner of 
RUN chown -R app-user:app-user /opt/docker-exercises-app
#switch to the user so the app starts as this user
USER app-user
# set default dir so that next commands executes in /home/docker-exercises-app
#all commands run after this point will run from this diretory
WORKDIR /opt/docker-exercises-app
#got the "javac" command from the Docker Hub document for the Java image
#RUN javac docker-exercises-project-1.0-SNAPSHOT.jar - nt required 

CMD ["java", "-jar", "docker-exercises-project-1.0-SNAPSHOT.jar"]